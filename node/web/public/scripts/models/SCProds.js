Hilary.scope('heinz').register({
    name: 'SCProds',
    singleton: true,
    dependencies: ['ko', 'SCProd', 'exceptions','router', 'jQuery'],
    factory: function (ko, SCProd, exceptions, router, $) {
        'use strict';
        var tax = 0.05;
        var SCProds = function (screcord) {
            var self = {};
            self.scprods = [];//ko.observableArray();

            self.tax = ko.observable();
            self.total = ko.observable();

            self.totalQuantity = ko.observable(screcord.totalQuantity);
            self.addSCProd = function (product) {
                if (!product) {
                    exceptions.throwArgumentException('The argument, scprod, must be defined to add a scprod', 'scprod');
                    return;
                }

                self.scprods.push(new SCProd(product));
            };

            // TODO: (Optimization) By adding items to the observableArray one
            // at a time, significantly more compute is required than if we
            // add them to a JS array and then set the value of self.books.
            self.loadRecords = function (screcord) {
                if (!screcord) {
                    exceptions.throwArgumentException('The argument, screcord, must be defined to add items', 'screcord');
                    return;
                }
                self.email = screcord.email;
                self.time = screcord.time;
                self.subtotal = parseFloat(screcord.subtotal).toFixed(2);
                self.tax = parseFloat(tax * self.subtotal).toFixed(2);
                self.total = (parseFloat(self.subtotal) + parseFloat(self.tax)).toFixed(2);

                var i = 0;
                if (screcord.items != []) {
                  for (i = 0; i < screcord.items.length; i += 1) {
                      self.addSCProd(screcord.items[i]);
                  }
                }

            };
            //TODO FIXME: currently the user value is default: shopper1
            self.placeOrder = function() {

              // get the current log status again from the back end system
              $.ajax({
                  url: '/api/logstatus',
                  method: 'GET'
              }).done(function (data) {

                if (data.status === false ) {
                  alert('Please sign in before checkout!!!!');
                } else {
                  console.log('SCProds: PlaceOrder get called');

                  // get the current amount
                  var checkoutItems = [];
                  var totalQuantity = 0;
                  var checkoutAmount = parseFloat($('#cart-total')[0].innerHTML);
                  // console.log(checkoutAmount);
                  // $('#cart-subtotal').html(subtotal.toFixed(2));
                  // console.log(self.scprods);
                  for (var i = 0; i < self.scprods.length; i++) {
                    if (self.scprods[i].isCheckOutNow === true) {
                      checkoutItems.push(self.scprods[i].prod);
                      totalQuantity += parseInt(self.scprods[i].prod.quantity);
                    }
                    // console.log(self.scprods[i].isCheckOutNow + ' ************ ' + self.scprods[i].prod.quantity);
                  }

                  console.log(checkoutItems);

                  // Don't actually know how to use the knockout here.
                  // let's use the jQuery to play as a workaround

                  // remove the items from the shopping cart
                  var quantity = -parseInt(totalQuantity);
                  var htm = document.getElementById('itemnumber');
                  htm.textContent = parseInt(htm.textContent) + parseInt(quantity);

                  // update the badge number of the items in the shopping cart
                  var uid = Date.now();
                    var orderToSend = {
                      uid: uid,
                      userEmail: data.useremail,
                      time: Date(),
                      flag: false,
                      items: JSON.stringify(checkoutItems),
                      total: checkoutAmount
                    };
                    // console.log(orderToSend.time);
                    console.log(orderToSend);
                    router.post('/order/placeOrder', orderToSend);

                    console.log('===Send amount to payment');
                    var dataToSend = {
                        orderID: uid,
                        amount:self.total
                    };
                    // console.log('QQQQQQQQQQ');
                    // console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
                    // console.log(checkoutItems);
                    // console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
                    router.post('/removefromsc', checkoutItems);
                    console.log(dataToSend);
                    router.post('/payment', dataToSend);

                    return true;
                }
              });

              return true;
            };

            if (screcord) {
              self.loadRecords(screcord);
            }

            return self;
        };
      return SCProds;
  }
});
