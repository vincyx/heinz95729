Hilary.scope('heinz').register({
    name: 'SCProd',
    singleton: true,
    dependencies: ['ko', 'Product', 'jQuery', 'router'],
    factory: function (ko, Product, $, router) {
        'use strict';

        var fadeTime = 300;
        var taxRate = 0.06;
        var shippingRate = 0.1;

        var SCProd = function (prod) {
            var self = new Product(prod);
            self.prod = prod;
            // the only thing more is the quantity.
            self.quantity = prod.quantity;
            self.isBoxChecked = ko.observable(true);
            self.isCheckOutNow = true;
            self.quantity = ko.observable(prod.quantity);
            self.linePrice = ko.computed(function() {
              return (self.price() * self.quantity()).toFixed(2) ;
            });

            self.onQuantityChanged = function() {
              var productRow = $('.scprod');
              console.log('on quantity changed');
              // find the index of the current block in the following function
              // for each of the line
              var subtotal = 0;
              productRow.each(function () {
                var curTitle = ($(this).children().children('.scprod-title'))[0].innerHTML;
                var curPrice = ($(this).children('.scprod-price'))[0].innerHTML;
                var curQuant = ($(this).children('.scprod-quantity')[0].children.quantity.value);

                  if (curTitle === prod.title) {
                    // update the price for this product.
                      var tprice = parseFloat(curPrice);
                      var tquantity = parseFloat(curQuant);
                      var linePrice = tprice * tquantity;

                      var obj = $(this).children('.scprod-line-price');
                      console.log(obj);
                      obj.fadeOut(fadeTime, function() {
                              // obj.text(linePrice.toFixed(2));
                              // TODO FIXME: How to use knock out to achieve the animation??
                              // we use a simple work around here... though this is very ugly..
                              obj.fadeIn(fadeTime);
                      });

                      var quantity = -parseInt(prod.quantity) + parseInt(tquantity);
                      var htm = document.getElementById('itemnumber');
                      htm.textContent = parseInt(htm.textContent) + parseInt(quantity);

                      subtotal += linePrice;
                      self.prod.quantity = tquantity;
                      console.log(prod.quantity);
                      router.navigate('/shoppingcart/update/'+prod.uid+'/'+tquantity);
                  } else {
                     subtotal += parseFloat(curPrice) * parseFloat(curQuant);
                  }
                });

                recalculateCart(subtotal);
                return true;
            };

            // when the remove is triggered.
            self.onRemove = function() {
              var productRow = $('.scprod');
              // find the index of the current block in the following function
              // for each of the line
              var subtotal = 0;
              // var i = 0;
              productRow.each(function () {
                var curTitle = ($(this).children().children('.scprod-title'))[0].innerHTML;
                var curPrice = ($(this).children('.scprod-price'))[0].innerHTML;
                var curQuant = ($(this).children('.scprod-quantity')[0].children.quantity.value);

                if (curTitle === prod.title) {
                    $(this).slideUp(fadeTime, function(){
                      $(this).remove();
                    });

                    // remove it from the current list
                    // this is a work around since we are in the scope of
                    // scprod only instead of in the scope of scprods.
                    var quantity = -parseInt(prod.quantity);
                    var htm = document.getElementById('itemnumber');
                    htm.textContent = parseInt(htm.textContent) + parseInt(quantity);

                    self.prod.quantity = 0;
                    router.navigate('/shoppingcart/update/'+prod.uid+'/0');
                } else {
                   subtotal += parseFloat(curPrice) * parseFloat(curQuant);
                }
                // i+=1;
              });
              recalculateCart(subtotal);
            };

            // when the box unselects.
            self.onCheckBoxClicked = function() {

              var productRow = $('.scprod');
              // find the index of the current block in the following function
              // for each of the line
              var subtotal = 0;
              var i = 0;
              productRow.each(function () {
                var curTitle = ($(this).children().children('.scprod-title'))[0].innerHTML;
                var curPrice = ($(this).children('.scprod-price'))[0].innerHTML;
                var curQuant = ($(this).children('.scprod-quantity')[0].children.quantity.value);
                // var curBox = ($(this).children('.scprod-check')[0].firstElementChild.checked);

                if (curTitle === prod.title) {
                    $(this).children('.scprod-check')[0].firstElementChild.checked = !self.checked;
                    // curBox.checked = !curBox.checked;
                    // console.log(ckboxes[i]);
                    // console.log(self.checked);
                    // console.log($(this).children('.scprod-check')[0].firstElementChild.checked);
                    // console.log($(this).children('.scprod-check'));
                    // TODO FIXME: remove this object from the order list.

                    if(self.isBoxChecked() === false) {
                      subtotal += parseFloat(curPrice) * parseFloat(curQuant);
                      self.isBoxChecked(true);
                      self.isCheckOutNow = true;
                      console.log(self.isBoxChecked());
                    } else {
                      self.isCheckOutNow = false;
                      self.isBoxChecked(false);
                      console.log(self.isBoxChecked());
                    }
                } else {
                    subtotal += parseFloat(curPrice) * parseFloat(curQuant);
                }
                });
                recalculateCart(subtotal);
                i+=1;

                return true;
            };

            // TODO FIXME: use the self.quantity to get the numerical value of the quantity and recompute the money.
            self.getLinePrice = function() {
                return (prod.price * prod.quantity).toFixed(2);
            };

            // a helper function. To update the subtotal of the order.
            /* Recalculate cart */
            function recalculateCart(subtotal)
            {
              console.log('Recalculating cart!!!');

              /* Calculate totals */
              var tax = subtotal * taxRate;
              var shipping = (subtotal > 0 ? shippingRate : 0);
              var total = subtotal + tax + shipping;

              /* Update totals display */
              $('.totals-value').fadeOut(fadeTime, function() {
                $('#cart-subtotal').html(subtotal.toFixed(2));
                $('#cart-tax').html(tax.toFixed(2));
                $('#cart-shipping').html(shipping.toFixed(2));
                $('#cart-total').html(total.toFixed(2));
                if(total <= 0){
                  $('.place-order').fadeOut(fadeTime);
                }else{
                  $('.place-order').fadeIn(fadeTime);
                }
                 $('.totals-value').fadeIn(fadeTime);
              });
            }
            return self;
        };

        return SCProd;
    }
});
