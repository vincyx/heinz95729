Hilary.scope('heinz').register({
    name: 'Book',
    singleton: true,
    dependencies: ['ko', 'Product', 'jQuery', 'router'],
    factory: function (ko, Product, $, router) {
        'use strict';

        var Book = function (book) {
            var self = new Product(book);

            self.thumbnailLink = ko.observable(book.thumbnailLink || '/images/books/default.png');
            self.reviews = ko.observableArray();

            self.purchase = function() {
              // first read the quantity.
              var quantity = $('.book-quantity input').val();
              var htm = document.getElementById('itemnumber');
              htm.textContent = parseInt(htm.textContent) + parseInt(quantity);

              // TODO FIXME: add this product to the shoppign cart
              router.navigate('/shoppingcart/increment/'+book.uid+'/'+quantity);
              return true;
            };

            return self;
        };


        return Book;
    }
});
