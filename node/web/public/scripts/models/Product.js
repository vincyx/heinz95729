Hilary.scope('heinz').register({
    name: 'Product',
    singleton: true,
    dependencies: ['router', 'ko', 'Blueprint', 'exceptions', 'jQuery'],
    factory: function (router, ko, Blueprint, exceptions, $) {
        'use strict';

        var blueprint,
            Product;

        blueprint = new Blueprint({
            title: 'string',
            description: 'string',
            metadata: {
                type: 'object',
                required: false
            },
            price: 'money',
            images: {
                type: 'array',
                required: false
            },
            thumbnailLink: {
                type: 'string',
                required: false
            }
        });

        Product = function (product) {
            var self = {};

            if (!blueprint.syncSignatureMatches(product).result) {
                exceptions.throwArgumentException('A product argument is required to create a new Product', 'product', blueprint.syncSignatureMatches(product).errors);
                return;
            }

            product = product || {};

            var type = product.type || 'product';

            self.uid = ko.observable(product.uid);
            self.title = ko.observable(product.title || undefined);
            self.description = ko.observable(product.description || undefined);
            self.metadata = ko.observable(product.metadata || undefined);
            self.price = ko.observable(product.price || undefined);
            self.images = ko.observableArray();
            self.thumbnailLink = ko.observable(product.thumbnailLink || '/images/products/default.png');

            self.thumbnailAlt = ko.computed(function () {
                return 'thumbnail for ' + self.title();
            });
            self.detailsLink = ko.computed(function () {
                return '/' + type + '/' + self.uid();
            });

            // Ensure updates no more than once per 50-millisecond period
            self.thumbnailAlt.extend({ rateLimit: 50 });
            self.detailsLink.extend({ rateLimit: 50 });

            self.click = function () {
                router.navigate(self.detailsLink());
            };

            // add the item to the shopping cart.
            // this will also updates the number indicator between the shopping cart logo.
            self.addToCart = function() {
              // first read the quantity.
              var productRow = $('.caption');
              // for each of the line
              productRow.each(function () {
                // console.log($(this));
                var curTitle = $(this).children()[0].innerText;
                curTitle = ($(this).children()[0].innerText.substring(0, curTitle.length-1));
                var curQuant = ($(this).children('.quantity')[0].children.val.value);

                if (curTitle.localeCompare(product.title) === 0) {
                    // update the price for this product.
                    // var quantity = $('.book-quantity input').val();
                    var quantity = parseFloat(curQuant);
                    // console.log(quantity);

                    var htm = document.getElementById('itemnumber');
                    htm.textContent = parseInt(htm.textContent) + parseInt(quantity);
                    // console.log(htm);

                    // TODO FIXME: add this product to the shopping cart.
                    // TODO FIXME: also update the product.
                    router.navigate('/shoppingcart/increment/'+product.uid+'/'+quantity);
                }
              });

              return true;
            };

            return self;
        };

        return Product;

    }
});
