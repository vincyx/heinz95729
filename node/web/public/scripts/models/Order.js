Hilary.scope('heinz').register({
    name: 'Order',
    singleton: true,
    dependencies: ['router', 'ko', 'Blueprint','jQuery', 'exceptions'],
    factory: function (router, ko, Blueprint,$, exceptions) {
        'use strict';

        var blueprint,
            Order;

        blueprint = new Blueprint({
            uid: 'String',
            description: 'String',
            userEmail: 'string',
            time: 'string',
            flag: 'bool',
            items: {
              type: 'array',
              required: false
            },
            total: 'number'
        });
        Order = function (order) {
            var self = {};

            if (!blueprint.syncSignatureMatches(order).result) {
                exceptions.throwArgumentException('An order argument is required to create a new order', 'order', blueprint.syncSignatureMatches(order).errors);
                return;
            }
            order = order || {};
            var type = order.type || order;
// this is where the defined variables come from the domain
            self.uid = ko.observable(order.uid);
            self.userEmail = ko.observable(order.userEmail);
            self.time = ko.observable(order.time);
            self.flag = ko.observable(order.flag);
            self.items = order.items;
            self.total = order.total||0;

            self.detailsLink = ko.computed(function () {
                return '/order/' + self.uid();
            });

            self.orderListLink = ko.computed(function () {
                var index = self.userEmail().indexOf('@');
                var user = self.userEmail().substring(0,index);
                return '/orders/' + user;
            });


            self.click = function () {
                router.navigate(self.detailsLink());
            };
            self.click1 = function () {
                router.navigate(self.orderListLink());
                return true;
            };

            return self;
        };
        return Order;
    }
});
