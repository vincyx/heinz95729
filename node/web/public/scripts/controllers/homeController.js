Hilary.scope('heinz').register({
    name: 'homeController',
    dependencies: ['newGidgetModule', 'GidgetRoute', 'locale', 'viewEngine', 'Products', 'SCProds', 'jQuery'],
    factory: function ($this, GidgetRoute, locale, viewEngine, Products, SCProds, $) {
        'use strict';

        $this.get['/'] = new GidgetRoute({
            routeHandler: function (err, req) {
                $.ajax({
                    url: '/api/logstatus',
                    method: 'GET'
                }).done(function (data) {
                  viewEngine.setVM({
                      template:
                       't-empty',
                      data: {
                          heading: locale.pages.home.empty.heading,
                          body: locale.pages.home.empty.body
                      }
                  });
                  console.log('In the homepage, recieved the returned value from the logstate check');
                  console.log(data);

                  var htm = document.getElementById('itemnumber');
                  htm.textContent = parseInt(data.quantity);

                  if(data.status === true) {
                    console.log('The front end recieves the log status: there is already a user logged in!!!!');
                    $('#loginclick').fadeOut();
                    $('#logoutclick').fadeIn();
                  } else {
                    $('#logoutclick').fadeOut();
                    $('#loginclick').fadeIn();
                  }
                });
            }
        });

        // GET /#/search/?q=searchterm
        // search for products
        $this.get['/search'] = new GidgetRoute({
            routeHandler: function (err, req) {
                $.ajax({
                    url: '/api/search?q=' + req.uri.query.q,
                    method: 'GET'
                }).done(function (data) {
                  // console.log('Hello world????');
                    var results = new Products(data);
                    if (results.products().length > 0) {
                        viewEngine.setVM({
                            template: 't-product-grid',
                            data: results
                        });
                    } else {
                        viewEngine.setVM({
                            template: 't-no-results',
                            data: { searchterm: req.uri.query.q }
                        });
                    }
                });
            }
        });

        $this.get['/checkout'] = new GidgetRoute({
              routeHandler: function (err, req) {
                  console.log('homecontroller checkout get called:');
                  $.ajax({
                      url: '/api/shoppingcart/checkout',
                      method: 'GET'
                  }).done(function (data) {
                    console.log(data);
                      // var results = new SCProds(data);
                      // console.log(results);
                      // if (data.items.length > 0) {
                        var results = new SCProds(data);
                          viewEngine.setVM({
                              template: 't-shoppingcart',
                              data: results
                          });
                      // } else {
                      //     viewEngine.setVM({
                      //         template: 't-no-results'
                      //     });
                      // }
                  });
              }
          });

        // update quantity. by yule
        // TODO: how to get user information and where to call this function???
        $this.get['/shoppingcart/update/:uid/:quantity'] = new GidgetRoute ({
          routeHandler: function (err, req) {
              console.log('shoppingcartController gets called');
              $.ajax({
                  url: '/api/shoppingcart/update/'+req.params.uid+'/'+req.params.quantity,
                  method: 'GET'
              }).done(function (data) {
                  console.log(data);
              });
          }
        });

        // update quantity. by yule
        // TODO: how to get user information and where to call this function???
        $this.get['/shoppingcart/increment/:uid/:quantity'] = new GidgetRoute ({
          routeHandler: function (err, req) {
              console.log('shoppingcartController gets called');
              $.ajax({
                  url: '/api/shoppingcart/increment/'+req.params.uid+'/'+req.params.quantity,
                  method: 'GET'
              }).done(function (data) {
                console.log('Client get the response from the server with the increment item quantity function.');
                  console.log(data);
              });
          }
        });

        $this.post['/removefromsc'] = new GidgetRoute({
            routeHandler: function (err, req) {
                $.ajax({
                    url: '/api/removefromsc',
                    method: 'POST',
                    data: {data : JSON.stringify(req.payload)}
                }).done(function (data) {
                    console.log(data + '*****************************');
                });
            }
        });

        return $this;
    }
});
