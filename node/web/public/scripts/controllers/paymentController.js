Hilary.scope('heinz').register({
    name: 'paymentController',
    dependencies: ['router', 'newGidgetModule', 'GidgetRoute', 'locale', 'viewEngine', 'Products', 'jQuery', 'PaymentVM', 'Stripe'],
    factory: function (router, $this, GidgetRoute, locale, viewEngine, Products, $, PaymentVM, Stripe) {
        'use strict';

        var data = new PaymentVM();
        var amount=0;
        var uid;

        Stripe.setPublishableKey('pk_test_KypFz2TXEo1tNIpEzrQfxjxI');

        // create Stripe token on what event?
        $this.get['/payment'] = function () {
            viewEngine.setVM({
                template: 't-payment',
                data: data
            });
        };

        $this.post['/payment'] = function (err, req) {

            data.amount = '$' + req.payload.amount;
            amount = req.payload.amount;
            uid = req.payload.orderID;
            console.log("[paymentController]ahhahahahaha"+uid);

            viewEngine.setVM({
                template: 't-payment',
                data: data
            });
        };

        $this.post['/getToken'] = function (err, req) {
            console.log(req.payload);
            Stripe.createToken({
            number:req.payload.number,
            exp_month:req.payload.exp_month,
            exp_year:req.payload.exp_year,
            cvc:req.payload.cvc
            },
            function(err, token) {
                //console.log("token: ", token);
                if(err != 200){
                        viewEngine.setVM({
                            template: 't-payment-failed',
                            data: {}
                        });
                }else{
                    $.ajax({
                    url: '/api/payments?q='+ token.id + '&a=' + amount,
                    method: 'GET'
                }).done(function (data) {
                   console.log('Payment Succesful.');
                   console.log('response data:', data);

                   $.ajax({
                        url: '/api/order/'+uid,
                        method: 'GET',
                    }).done(function (data) {
                        console.log('done');
                        console.log('Nested Ajax call.....');
                        console.log('response data:', data.items);
                        //Nested Ajax call
                        $.ajax({
                            url: '/api/order/'+uid+'/updateFlag',
                            method: 'POST',
                            data: { items: JSON.stringify(data.items) }
                        }).done(function (data) {
                           console.log('Succeeded in updating MongoDB.');
                            //Direct user to order detail page.
                            console.log("Direct user to order detail page...");
                            console.log('/order/'+uid);
                            router.navigate('/order/' + uid);

                        }).fail(function() {
                           console.log('Failed updating flag in MongoDB.');
                        });

                    }).fail(function() {
                            console.log("Failed retrieving order detail from MongoDB.")
                    });

                }).fail(function() {
                               console.log('Failed updating flag in MongoDB.');
                });

                }


            }
          );
        };

        return $this;
    }
});
