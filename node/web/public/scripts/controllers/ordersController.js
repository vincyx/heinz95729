Hilary.scope('heinz').register({
    name: 'ordersController',
    dependencies: ['newGidgetModule', 'GidgetRoute', 'locale', 'viewEngine', 'jQuery', 'Orders', 'Order'],
    factory: function ($this, GidgetRoute, locale, viewEngine, $, Orders, Order) {
        'use strict';

        // GET single order
    $this.get['/order/:uid'] = new GidgetRoute({
            routeHandler: function (err, req) {
                $.ajax({
                  url: '/api/order/' + req.params.uid,
                  method: 'GET'
                }).done(function(data) {
                  var order = new Order(data);
                  viewEngine.setVM({
                      template: 't-order-detail',
                      data: {order: order}
                  });
                });
            },
        });

        // GET /oerders/:users
        // search for orders
        $this.get['/orders/:user'] = new GidgetRoute({
            routeHandler: function (err, req) {
                $.ajax({
                    url: '/api/orders/' + req.params.user,
                    method: 'GET'
                }).done(function (data) {
                    var orders = new Orders(data);
                    if (orders.orders().length > 0) {
                        viewEngine.setVM({
                            template: 't-order-list',
                            data: orders

                          });
                    } else {
                        viewEngine.setVM({
                            template: 't-no-order',
                            data: { user: req.params.user } // find how to get userid when it's stored from the order
                        });
                    }
                });
            }
        });
        /*
        POST: /order/placeorder, place an order
        */
        $this.post['/order/placeorder'] = new GidgetRoute ({
            routeHandler: function (err, req) {
              console.log('OrderController: placeanorder');
              console.log(req.payload);
                $.ajax({
                    url: '/api/order/placeorder',
                    method: 'POST',
                    data: req.payload
                }).done(function (data) {
                  console.log(data);
                });
            }
        });
        return $this;
    }
});
