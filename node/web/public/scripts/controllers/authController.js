Hilary.scope('heinz').register({
    name: 'authController',
    dependencies: ['newGidgetModule', 'GidgetRoute', 'locale', 'viewEngine', 'router'],
    factory: function ($this, GidgetRoute, locale, viewEngine, router) {
        'use strict';

        // GET /#/login
        // login
        $this.get['/login'] = new GidgetRoute({
            routeHandler: function () {
              console.log('Start to render the log in page!!!!');
                viewEngine.setVM({
                    template: 't-login',
                    data: { }
                });
            }
        });

        // GET /logout
        // also print the message that the user has already logged out.
        // $this.get['/logout'] = new GidgetRoute({
        //       routeHandler: function () {
        //         console.log('Hey i am in the log out page here!');
        //         return true; // do nothing
        //     }
        // });

        // // POST /login
        // // login
        // $this.post['/login'] = new GidgetRoute({
        //     routeHandler: function () {
        //         return true; // ignore
        //     }
        // });

        // GET /register
        // Register a new account
        $this.get['/register'] = new GidgetRoute({
            routeHandler: function () {
                viewEngine.setVM({
                    template: 't-register',
                    data: {}
                });
            }
        });

        return $this;
    }
});
