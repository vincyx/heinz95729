Hilary.scope('heinz').register({
  name: 'localStorageController',
  dependencies: ['newGidgetModule'],
  factory: function ($this) {
    'use strict';
    if (!localStorage) {
      console.log('LocaleStorage is not supported, data will not be persisted');

      // Let the program use a stub object to proceed
      // localStorage = {};
      localStorage.prototype.removeItem = function(key) {
        this[key] = null;
      };
    }

    var store = {
      read: function(key) {
        if (localStorage[key]) {
          return JSON.parse(localStorage[key]);
        }
        return null;
      },

      write: function(key, value) {
        localStorage[key] = JSON.stringify(value);
      },

      clear: function(key) {
        localStorage.removeItem(key);
      }
    };

    // On load, look for an existing cart to preload
    var MyApp.getCart = function() {
      // Get the cart
      var cartItems = store.read('myapp.cart');
      var checkoutCounter = cartItems.count;
      // Update the button counter value
      counterSpan.text(checkoutCounter);
      // External function to enable the button
      MyApp.theCounter();
    };

     var ShoppingCart = function(name) {
      this.name = name;
      this.items = store.read(name);
    };

    ShoppingCart.prototype = {
      save: function () {
        store.write(this.name, this.items);
      },


      addItem: function (role) {
        if (this.items[role]) {
        this.items[role] += 1;
      }
        else {
        this.items[role] = 1;
}
        this.save();
      },

      removeItem: function(role) {
        if (this.items[role]) {
        this.items[role] -= 1;
}
        if (this.items[role] == 0) {
        delete this.items[role];
}
        this.save()
      }

    }

    var myCart = new ShoppingCart('myapp.cart');




    return $this;
  }
});
