Hilary.scope('heinz').register({
    name: 'PaymentVM',
    dependencies: ['jQuery', 'ko', 'router'],
    factory: function ($, ko, router) {
        'use strict';

        var PaymentVM;

        PaymentVM = function() {
            var self = {};

            self.card = ko.observable('4242424242424242');
            self.cvc = ko.observable('123');
            self.month = ko.observable('05');
            self.year = ko.observable('2017');
            
            self.makePayment = function() {
                var dataToSend = {
                    number:self.card(),
                    exp_month:self.month(),
                    exp_year:self.year(),
                    cvc:self.cvc()
                }

                router.post('/getToken', dataToSend);
            };
            
            return self;
        };

        return PaymentVM;
    }
});