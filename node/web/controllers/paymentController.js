module.exports.name = 'paymentController';
module.exports.dependencies = ['router','request'];
module.exports.factory = function (router,request) {
    'use strict';
    //charge shopper using token using using stripe
    router.get('/api/payments', function (req, res) {
        console.log(req.cookies.auth._id);
        var dataToSend = {
            form: {
                amount: parseFloat(req.query.a)*100,
                currency:'usd',
                source:req.query.q,
                description: 'Charge customer'
            },
            auth: {
                user: 'sk_test_7a408TnZe6B9A0JPputMmFan',
                pass:''
            },
            method: 'POST',
            url: 'https://api.stripe.com/v1/charges'
        };
        
        request(dataToSend, function(err, HttpRes, body) {
            if(!err){
                    if (HttpRes.statusCode === 200) {
                        res.send('200');                        
                }
            }            
            else {
                res.send('400');
            }
        });        
    });

    return router;
};