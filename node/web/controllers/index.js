module.exports = [
    require('./homeController.js'),
    require('./authController.js'),
    require('./paymentController.js'),
    require('./shoppingcartController.js')
];
