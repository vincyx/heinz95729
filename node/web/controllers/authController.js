module.exports.name = 'authController';
module.exports.dependencies = ['router', 'usersRepo', 'shoppingcartsRepo', 'exceptions'];
module.exports.factory = function (router, repo, screpo, exceptions) {
  'use strict';

  var authCookieExpiryDurationMinutes = 43200, // 30 days
  maxAge = authCookieExpiryDurationMinutes * 60 * 1000,
  addCookie;

  addCookie = function (user, res) {
    // normally, you wouldn't set a plain old user object as the
    // value of the cookie - it's not secure.
    res.cookie('auth', user, { maxAge: maxAge, httpOnly: true });
  };

  router.get('/api/logstatus', function(req, res) {
    console.log('Recieving the request asking for the log status...');
    console.log(req.cookies);
    if(req.cookies.auth === undefined) {
      // the user is not logged in
      // check the local cookie for total item number
      var yule = req.cookies.yule;
      var totalquantity = parseInt(0);
      if (yule !== undefined) {
        for(var i = 0; i < yule.length; i++) {
          totalquantity += parseInt(yule[i].quantity);
        }
      }

      res.send({status:false, quantity:totalquantity, useremail: null});
    } else {
      console.log(req.cookies.auth);

      screpo.get(req.cookies.auth.email, function(err, result) {
        if (err) {
          exceptions.throwException(err);
          res.status(400);
          return;
        }
        res.send({status:true, quantity: result.totalQuantity, useremail:req.cookies.auth.email});
      });
    }
  });

  router.post('/register', function (req, res) {
    repo.create(req.body, function (err, result) {
      if (!err && result.insertedId) {
        repo.get(req.body.email, function (err, user) {
          if (!err) {
            addCookie(user, res);
            res.redirect('/');
          } else {
            res.status(400);
          }
        });
      } else {
        res.status(400);
      }
    });
  });

  router.post('/login', function (req, res, next) {
    console.log('Server is trying to login....');
    console.log(req.body);
    repo.get(req.body.email, function (err, user) {
      if (user.email !== undefined) {
        console.log(user);
        console.log('User is authenticated, adding this user to the local cookie...');
        addCookie(user, res);
        // console.log('This is : '+req.cookies.auth.name + ' ' + req.cookies.auth.email);

        // clear the local data for the shoppign cart
        // feed the data to the backend database under this user
        var yule = req.cookies.yule;
        if (yule !== undefined) {
          // fetch the data
          screpo.updateItemsToDB(res, yule, user, false);
          // screpo.updateItemsToDB(res, yule.items, user, false);
          // for (var i = 0; i < yule.items.length; i++) {
          //   //req, res, uid, quantity, isReset, synchronizer
          //   screpo.updateItemQuantity(req, res, yule.items[i].uid, yule.items[i].quantity, false, user);
          // }
          // clear the cookie!! Remember this!!
          res.clearCookie('yule');
        }

        console.log('Sending returned value to the client side...');
        res.redirect('/');
      } else {
        console.log('The user is a ghost...');
        // res.send('@@@@');
        // res.redirect('/login');
        next();
      }
    });
  });

  router.get('/logout', function (req, res) {
    // clear the cookie of the auth
    res.clearCookie('auth');
    console.log('The auth is logged out~!!!!');

    res.redirect('/');
    // res.send(false);
  });

  return router;
};
