module.exports.name = 'Order';
module.exports.dependencies = ['Blueprint', 'exceptions', 'ObjectID'];
module.exports.factory = function (Blueprint, exceptions, ObjectID) {
    'use strict';

    var blueprint,
        Order;

    blueprint = new Blueprint({
        _id: {
            type: 'object',
            required: false
        },
	      uid: 'String',
        description: 'String',
        userEmail: 'string',
        time: 'string',
        flag: 'bool',
        items: {
            type: 'array',
            required: false
        },
        total: 'number'
    });

    Order = function (order) {
        console.log("~~~~~~~~~~~~~~~*&*&*&*&*&*&*&*&*");
        console.log(order);
        var self = {};
        console.log('rebuild an order object');
        if (!blueprint.syncSignatureMatches(order).result) {
            exceptions.throwArgumentException('', 'order', blueprint.syncSignatureMatches(order).errors);
            return;
        }
        if(order._id) {
          self._id = new ObjectID(order._id);
        }
        else {
          console.log('create an object id');
          self._id = new ObjectID();
        }
	    self.uid = order.uid;
        self.userEmail = order.userEmail;
        self.time = order.time;
        self.flag = order.flag;
        self.items = order.items;
        self.total = order.total;

        return self;
    };

    Order.db = {
        collection: 'orders',
        indexes: [

            {
                keys: { _id: 1 },
                options: { name: 'unq.orders.orderid', unique: true, sparse: true }
            },

            // We use this index to filter queries by userid.
            {
                keys: {userEmail: 1},
                options: { name: 'unq.orders.userEmail', background: true}
            },
        ]
    };

    return Order;
};
