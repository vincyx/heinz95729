module.exports.name = 'ordersRepo';
module.exports.singleton = true;
//module.exports.blueprint = ['repoBlueprint'];
module.exports.dependencies = ['db', 'Order', 'Blueprint', 'exceptions', 'is'];
module.exports.factory = function (db, Order, Blueprint, exceptions, is) {
    'use strict';

    var self = {
            get: undefined,
            find: undefined,
            create: undefined,
            update: undefined,
            remove: undefined
        },
        collection = db.collection(Order.db.collection),
        findOptionsBlueprint, // for what???
        i;

    // ensure the indexes exist
    for (i = 0; i < Order.db.indexes.length; i += 1) {
        collection.createIndex(Order.db.indexes[i].keys, Order.db.indexes[i].options);
    }

    // seems i am not going to use this = =
    findOptionsBlueprint = new Blueprint({
        query: 'object',
        skip: {
            type: 'number',
            required: false
        },
        limit: {
            type: 'number',
            required: false
        }
    });

    /*
    // Get a single order
    */
     self.get = function (uid, callback) {

        // Blueprint isn't helpful for defending arguments, when they are
        // not objects. Here we defend the function arguments by hand.
        if (is.not.string(uid)) {
            exceptions.throwArgumentException('', 'uid');
            return;
        }

        if (is.not.function(callback)) {
            exceptions.throwArgumentException('', 'callback');
            return;
        }

        // Find order by uid
        collection.find({uid:uid}).limit(1).next(function (err, doc) {
            if (err) {
                callback(err);
                return;
            }
            console.log(doc);
            callback(null, new Order(doc));
        });
    };

    /*
    // Find order(s) by user email
    */
    self.find = function (user, callback) {
      // validate the function arguments
      if (is.not.string(user)) {
          exceptions.throwArgumentException('', 'user');
          return;
      }

      if (is.not.function(callback)) {
          exceptions.throwArgumentException('', 'callback');
          return;
      }

      // set default skip and limit values if they weren't set
      var skip = 0,
          limit =  20;

      //Find orders list by user email
      //TODO FIXME: change the input user into email.
      collection.find({userEmail: user+'@95729.com'}).skip(skip).limit(limit).toArray(function(err, docs) {
          var orders = [],
              i;

          if (err) {
              callback(err);
              return;
          }

          for (i = 0; i < docs.length; i +=1) {
              orders.push(new Order(docs[i]));
          }

          callback(null, orders);
      });
    };

    /*
    // Create an order
    */
    self.create = function (payload, callback) {

      console.log('OrderRepo create called');

      // Validate the function arguments
      // TODO FIXME: payload need to be validated.
      if (is.not.function(callback)) {
          exceptions.throwArgumentException('', 'callback');
          return;
      }
        collection.insertOne(payload, callback);

    };

    /*
     * 
     */
     self.updateFlag = function (uid, items, callback) {
        console.log("[orderRepo] enter update flag");
        collection.update(
            { uid : uid },
            { $set: { items : items, flag : true} },
            { upsert : false },
            function (err, docs) {
              if(err) {
                callback(err);
                return;
              }
              callback(null, docs);
            }
        );        
    };

    return self;
};
