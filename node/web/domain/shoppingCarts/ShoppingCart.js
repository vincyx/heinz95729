module.exports.name = 'Shoppingcart';
module.exports.dependencies = ['Blueprint', 'exceptions', 'ObjectID'];
module.exports.factory = function (Blueprint, exceptions, ObjectID) {
    'use strict';

    var blueprint,
        Shoppingcart;

    blueprint = new Blueprint({
        _id: {
            type: 'object',
            required: false
        },
        email: 'string',
        time: 'string',
        // an item inherites from Book. add an additional attribute: quantity
        items: {
            type: 'array',
            required: false
        },
        subtotal: 'number',
        totalQuantity: 'number'

    });

    Shoppingcart = function (shoppingcart) {
        var self = {};
        if (!blueprint.syncSignatureMatches(shoppingcart).result) {
            exceptions.throwArgumentException('', 'shoppingcart', blueprint.syncSignatureMatches(shoppingcart).errors);
            return;
        }

        self._id = new ObjectID(shoppingcart._id);
        self.email = shoppingcart.email;
        self.time = shoppingcart.time;
        self.items = shoppingcart.items;
        self.subtotal = shoppingcart.subtotal;
        self.totalQuantity = shoppingcart.totalQuantity;

        return self;
    };

    Shoppingcart.db = {
        collection: 'shoppingcarts',
        indexes: [
            //
            {
                keys: { _id: 1 },
                options: { name: 'unq.shoppingcarts._id', unique: true, sparse: true }
            },

            // We use this index to filter queries by userid.
            {
                keys: {email: 1},
                options: { name: 'unq.shoppingcarts.email', background: true}
            },
        ]
    };

    return Shoppingcart;
};
