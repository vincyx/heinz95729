module.exports.name = 'shoppingcartsRepo';
module.exports.singleton = true;
module.exports.dependencies = ['db', 'Shoppingcart', 'Blueprint', 'exceptions', 'is', 'productsRepo'];
module.exports.factory = function (db, Shoppingcart, Blueprint, exceptions, is, products) {
  'use strict';

  var self = {
    get: undefined,
    find: undefined,
    create: undefined,
    update: undefined,
    remove: undefined,
  },
  collection = db.collection(Shoppingcart.db.collection),
  i;

  // ensure the indexes exist
  for (i = 0; i < Shoppingcart.db.indexes.length; i += 1) {
    collection.createIndex(Shoppingcart.db.indexes[i].keys, Shoppingcart.db.indexes[i].options);
  }

  /*
  // Get a single shoppingCart by username (the part before @)
  */
  self.get = function (email, callback) {
    console.log(email);
    console.log('repo get called');

    // Blueprint isn't helpful for defending arguments, when they are
    // not objects. Here we defend the function arguments by hand.
    // if (is.not.string(email)) {
    //   exceptions.throwArgumentException('', 'user');
    //   return;
    // }

    if (is.not.function(callback)) {
      exceptions.throwArgumentException('', 'callback');
      return;
    }
    // Find shoppingCart by user email
    collection.find({ email:email}).limit(1).next(function (err, doc) {
      if (err) {
        callback(err);
        return;
      }
      callback(null, new Shoppingcart(doc));
    });
  };


  //TODO: do we need this to get multiple shoppingcarts? If yes, change the search key to use it
  // otherwise just remove this part
  /*
  // Find shoppingCart(s) by user email
  */
  self.find = function (user, callback) {
    // validate the function arguments
    if (is.not.string(user)) {
      exceptions.throwArgumentException('', 'user');
      return;
    }

    if (is.not.function(callback)) {
      exceptions.throwArgumentException('', 'callback');
      return;
    }

    // set default skip and limit values if they weren't set
    var skip = 0,
    limit =  20;

    //Find shoppingCarts list by user email
    collection.find({email: user+'@95729.com'}).skip(skip).limit(limit).toArray(function(err, docs) {
      var shoppingCarts = [],
      i;

      if (err) {
        callback(err);
        return;
      }

      for (i = 0; i < docs.length; i +=1) {
        shoppingCarts.push(new Shoppingcart(docs[i]));
      }

      callback(null, shoppingCarts);
    });
  };

  /*
  // Create an shoppingCart
  */
  self.create = function (payload, callback) {
    console.log('repo create get called');
    // Validate the function arguments
    if (is.not.object(payload)) {
      exceptions.throwArgumentException('', 'payload');
      return;
    }

    if (is.not.function(callback)) {
      exceptions.throwArgumentException('', 'callback');
      return;
    }

    collection.insertOne(payload, function(err, docs) {
      if(err) {
        callback(err);
        return;
      }
      callback(null,docs);
    });
  };

  /*
  // Update a Shoppingcart with new quantity
  */
  // TODO: how to combine different update methods? i tried to use different
  // payload to change the update field, but it doesn't work
  self.update = function (email, payload, callback) {
    console.log('repo update get called');
    console.log(payload);
    // Validate the function arguments
    if (is.not.string(email)) {
      exceptions.throwArgumentException('', 'user');
      return;
    }

    // TODO FIXME: payload need to be validated.

    if (is.not.function(callback)) {
      exceptions.throwArgumentException('', 'callback');
      return;
    }

    var sub = 0,
    index = 0,
    totalQ = 0;
    for(index = 0; index < payload.length; index++) {
      sub += payload[index].quantity * payload[index].price;
      totalQ += parseInt(payload[index].quantity);
    }

    collection.update(
      { email : email },
      { $set: { items : payload ,subtotal : sub, totalQuantity: totalQ } },
      { upsert : false },
      function (err, docs) {
        if(err) {
          callback(err);
          return;
        }
        callback(null, docs);
      }
    );
  };


  /*
  // Remove a Shoppingcart record
  */
  self.remove = function (email, callback) {
    // Validate the function arguments
    if (is.not.string(email)) {
      exceptions.throwArgumentException('', 'user');
      return;
    }

    if (is.not.function(callback)) {
      exceptions.throwArgumentException('', 'callback');
      return;
    }

    collection.remove({ userEmail: email}, function (err, docs) {
      if(err) {
        callback(err);
        return;
      }
      callback(null,docs);
    });
  };

  var authCookieExpiryDurationMinutes = 43200, // 30 days
  maxAge = authCookieExpiryDurationMinutes * 60 * 1000;

  // save the data to the anonymous guest cookie
  self.addDataCookie = function(data, res) {
    // normally, you wouldn't set a plain old user object as the
    // value of the cookie - it's not secure.
    res.cookie('yule', data, { maxAge: maxAge, httpOnly: true });
    console.log('The addDataCookie function is called here. After this the cookie should be added..');
  };

  self.updater = function(res, email, payload) {

    self.update(email, payload, function (err, docs) {
      if(err) {
        exceptions.throwException(err);
        res.status(400);
        return;
      }
      // res.send('Success'); // this is not neccesary at all!!!
    });
  };

  self.updateItemsToDB = function(res, items, user, isReset) {
    // if yes, then get the shoppingcart of the user
    console.log('****updating the items to the database..' + '\n' + items);
    // put the items to a hashmap for fast lookup
    // uid, item
    var hashmap = {};

    for(var i = 0; i < items.length; i++) {
      hashmap[items[i].uid] = items[i];
    }

    self.get(user.email, function(err, result) {
      if (err) {
        exceptions.throwException(err);
        res.status(400);
        return;
      }
      var shoppingcart = result;
      var payload = shoppingcart.items,
      index,
      item;
      // console.log(result);

      for (index = 0; index < payload.length; index++) {
        item = payload[index];
        // if the item already exists in the shoppingcart,update the quantity only
        if (hashmap.hasOwnProperty(item.uid)) {
          if(parseInt(hashmap[item.uid].quantity) === 0) {
            // remove this object.
            payload.splice(index,1);
          } else {
            if(isReset === true) {
              payload[index].quantity = hashmap[item.uid].quantity;
            } else {
              payload[index].quantity = parseInt(payload[index].quantity) + parseInt(hashmap[item.uid].quantity);
            }
          }
          delete hashmap[item.uid];
        }
      }

      // console.log(hashmap);
      // for the remaining:
      var uids = [];
      for(var key in hashmap) {
        if (hashmap.hasOwnProperty(key)) {
            uids.push(hashmap[key].uid);
          }
      }

      console.log('######adding a new elements to the payload body');
      products.getMany(uids, function (err, books) {
        if(err) {
          exceptions.throwException(err);
          res.status(400);
          return;
        }

        for(var i = 0; i < books.length; i++) {
            books[i].quantity = hashmap[books[i].uid].quantity;
            payload.push(books[i]);
        }
        self.updater(res, user.email, payload);
      });
  });
};


  // Attention:
  // FIXME: the user saved in the local cookie should always be the name.
  self.updateItemQuantity = function(req, res, uid, quantity, isReset) {
    console.log('shoppingCartApiController get called ' + uid + ' with quantity: ' + quantity);
    var shoppingcart;
    var user = req.cookies.auth;
    // first check if the user is already logged in or not
    if (user === undefined) {
      console.log('This is a guest!!!!');
      // res.cookie('yule', req);
      var anony = req.cookies.yule;
      var exists = false;
      if( anony === undefined) {
        // initialize the anonymous user cache
        console.log('********This is the first time that we register the YULE?');
        // anony = { email: 'yule', time:null, items: [], subtotal: parseFloat(0), totalquantity: parseInt(0)};
          anony = []; // since the cookie size is very limited, we only keep the quantity and the uid in local cookie.
        } else {
          // console.log(anony);
          // first check if the book is in the cookie or not
          for(var index = 0; index < anony.length; index++) {
            var item = anony[index]; // only stores the uid and the quantity.........
            var quan = parseInt(quantity);
            if(item.uid === uid) {
              exists = true;
              if(isReset === true) {
                // console.log((quan - parseInt(anony.items[index].quantity)) * parseFloat(anony.items[index].price));
                // anony.totalquantity += /*parseInt(anony.totalquantity) + */quan - parseInt(anony.items[index].quantity);
                // anony.subtotal += (quan - parseInt(anony.items[index].quantity)) * parseFloat(anony.items[index].price);
                anony[index].quantity = quan;

                if (quan === 0) {
                  // console.log('Current subtotal is :' + anony.subtotal);
                  anony.splice(index,1);
                }
              } else {
                anony[index].quantity = parseInt(anony[index].quantity) + quan;
                // anony.totalquantity += /*parseInt(anony.totalquantity) +*/ quan;
                // anony.subtotal += quan * parseFloat(anony.items[index].price);
              }
              break;
            }
          }
        }

        if(!exists) {
          console.log('************Adding one entry to the cookie...****************');
          // products.get(uid, function(err, book) {
          //   // console.log(book);
          //   if(err) {
          //     exceptions.throwException(err);
          //     res.status(400);
          //     return;
          //   }
          //   book.description = 'hi';
          //   book.quantity = quantity;
          //   anony.items.push(book);
          //   anony.totalquantity += /*parseInt(anony.totalquantity) +*/  parseInt(quantity);
          //   anony.subtotal += parseInt(quantity) * parseFloat(book.price);
          //   // console.log(anony);
          //   // console.log(anony.items.length);
          //   // save the information back to the cookie
          //   // res.clearCookie('yule');
          //   self.addDataCookie(anony, res);
          //   console.log('Successfully returned from the updateItemQuantity function with cookie mode!!');
          // });
          anony.push({uid:uid,
          quantity:quantity});
          console.log('***********************************************');
          console.log('quantity is ' + quantity);
          console.log('***********************************************');
        }
        self.addDataCookie(anony, res);
        console.log('Successfully returned from the updateItemQuantity function with cookie mode!!');
      } else {
        var email = user.email;
        // = user.email;

        console.log('Updating the shopping cart!!! with user already logged in! :');
        console.log(user);
        // if yes, then get the shoppingcart of the user
        self.get(email, function(err, result) {
          if (err || result === undefined) {
            exceptions.throwException(err);
            res.status(400);
            return;
          }

          shoppingcart = result;
          var payload = shoppingcart.items,
          index,
          item,
          exists = false;
          for (index = 0; index < payload.length; index++) {
            item = payload[index];
            // if the item already exists in the shoppingcart,update the quantity only
            if (item.uid === uid) {
              exists = true;
              if(parseInt(quantity) === 0) {
                payload.splice(index,1);
              } else {
                if(isReset === true) {
                  payload[index].quantity = quantity;
                } else {
                  payload[index].quantity = parseInt(payload[index].quantity) + parseInt(quantity);
                }
              }
              // console.log(payload);
              self.updater(res, email, payload);
            }

          }
          // if the item is not in the shoppingcart, get the item from
          // productsrepo first and add it to the shoppingcart
          if (!exists) {
            console.log('###########item not found in the users shopping cart. add a new one');
            products.get(uid, function (err, book) {
              if(err) {
                exceptions.throwException(err);
                res.status(400);
                return;
              }
              book.quantity = quantity;
              payload.push(book);
              // console.log(payload);
              self.updater(res, email, payload);
            });
          } else {
            console.log('************Add one extra item to the shopping cart..');
          }
        });
      }
    };
    return self;
  };
