module.exports.name = 'ordersApiController';
module.exports.dependencies = ['router', 'ordersRepo', 'exceptions', 'Order'];
module.exports.factory = function (router, repo, exceptions, Order) {
    'use strict';

    /*
    Get order by orderid.
    */
    router.get('/api/order/:uid', function (req, res) {
        repo.get(req.params.uid, function (err, order) {
            if (err) {
                exceptions.throwException(err);
                res.status(400);
                return;
            }
            res.send(order);
        });
    });

    // update flag for the order
    router.post('/api/order/:uid/updateFlag', function (req, res) {
        console.log("[OrderApiController] update flag");
        var uid = req.params.uid;
        var items = req.body.items;
        console.log(uid);        
        console.log(req.body.flag);
        var items = JSON.parse(req.body.items);        
        for(var i=0; i<items.length; i++) 
        {
                console.log("each item");
                console.log(items[i]);
                items[i].flag= true;
                console.log(items[i]);
        }
        console.log("=====before doing repo update flag====");
        console.log(items);
        repo.updateFlag(uid, items, function (err, order) {
            if (err) {
                exceptions.throwException(err);
                res.status(400);
                return;
            }
            res.send(200);
        });
    });
/*
Get orders by username. user name is the part before @ in the email.
*/
router.get('/api/orders/:user', function (req, res) {
    repo.find(req.params.user, function (err, order) {
        if (err) {
            exceptions.throwException(err);
            res.status(400);
            return;
        }
        res.send(order);
    });
});
/*
Create orders.
*/
router.post('/api/order/placeorder', function (req, res) {
    console.log('OrderApiController:Create an order');
    // preprocessing the order object.
    // and create a new order
    var oldOrder = req.body;
    console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    console.log(oldOrder);
    oldOrder.items = JSON.parse(oldOrder.items);
    oldOrder.flag = Boolean(oldOrder.flag);
    oldOrder.total = oldOrder.total * 1;
    var newOrder = new Order(oldOrder);

    repo.create(newOrder, function (err, docs) {
        if(err) {
          exceptions.throwException(err);
          res.status(400);
          return;
        }
        res.send('Success');
    });

});

return router;
};
