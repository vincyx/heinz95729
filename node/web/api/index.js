module.exports = [
    require('./searchApiController.js'),
    require('./booksApiController.js'),
    require('./orderApiController.js'),
    require('./shoppingcartApiController.js')
];
