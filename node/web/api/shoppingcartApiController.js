module.exports.name = 'shoppingcartApiController';
module.exports.dependencies = ['router', 'shoppingcartsRepo', 'productsRepo', 'exceptions'];
module.exports.factory = function (router, repo, products, exceptions) {
  'use strict';

  // update the shoppingcart
  router.get('/api/shoppingcart/update/:uid/:quantity',function(req, res, next) {
    repo.updateItemQuantity(req, res, req.params.uid, req.params.quantity, true);
    next();
  });

  ////
  //// the increment function
  ////
  router.get('/api/shoppingcart/increment/:uid/:quantity', function(req, res, next) {
    console.log('add some items to the shopping cart.');
    repo.updateItemQuantity(req, res, req.params.uid, req.params.quantity, false);
    console.log('returned from the increment item quantity function..');
    next();
  });

  router.get('/api/shoppingcart/checkout',function(req, res) {
    console.log('####shoppingCartApiController checkout gets called');

    // first check if the user is logged in or not
    if (req.cookies.auth === undefined) {
      console.log('Attempting to checkout without login, aborting!!');
      // then we should wrapper the data from the cookie and then return them

      var yule = req.cookies.yule;
      if (yule === undefined) {
        // initialize the yule structure
        // yule = { email: 'yule', time:null, items: [], subtotal: parseFloat(0), totalquantity: parseInt(0)};
        yule = [];
      }

      // TODO FIXME: wraparround the items here. Extract all the fields for all the books.
      var uids = [];
      var hashmap = {};
      for(var i = 0; i < yule.length; i++) {
        hashmap[yule[i].uid] = yule[i].quantity;
        uids.push(yule[i].uid);
      }

      var scRawData = { email : 'yule', time: null, items:[], subtotal: parseFloat(0), totalquantity:parseInt(0)};

      products.getMany(uids, function (err, books) {
        if(err) {
          exceptions.throwException(err);
          res.status(400);
          return;
        }
        for(var i = 0; i < books.length; i++) {
            books[i].quantity = hashmap[books[i].uid];
            // console.log(books[i]);
            scRawData.items.push(books[i]);
            scRawData.subtotal += parseFloat(books[i].price) * parseInt(books[i].quantity);
        }
        // console.log(scRawData);
        res.send(scRawData);
      });

    } else {

      console.log('Get the shopping cart of the specific user');
      console.log(req.cookies.auth);

      var shoppingcart;
      // get the shoppingcart of the user
      repo.get(req.cookies.auth.email, function(err, result) {
        if (err) {
          exceptions.throwException(err);
          res.status(400);
          return;
        }
        shoppingcart = result;
        console.log(shoppingcart);
        res.send(shoppingcart);
      });
    }
  });

  router.post('/api/removefromsc', function(req, res) {
    var hashmap = {};
    // console.log(req.payload);
    // console.log(req.body);
    var yule = JSON.parse(req.body.data);
    // console.log(JSON.parse(yule));
    // console.log(JSON.parse(req.body.data));
    // console.log('*************************************');
    //
    // console.log('*************************************');
    // console.log('*************************************');
    // console.log('*************************************');
    // // console.log(yule);
    // console.log('*************************************');
    // console.log('*************************************');
    // console.log('*************************************');
    // // console.log(req.body);
    // console.log('*************************************');
    // console.log('*************************************');

    for(var i = 0; i < yule.length; i++) {
      // console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@2');
      // console.log(yule[i].prod.uid);
      hashmap[yule[i].uid] = yule[i].quantity;
    }

    repo.get(req.cookies.auth.email, function(err, result) {
      if (err) {
        exceptions.throwException(err);
        res.status(400);
        return;
      }
      var payload = result.items;
      // console.log('*************************************');
      // console.log('*************************************');
      // console.log('*************************************');
      // console.log(payload);
      // console.log('*************************************');
      // console.log('*************************************');
      // console.log('*************************************');
      // console.log(payload.length);
      // console.log('*************************************');
      // console.log('*************************************');
      // console.log('*************************************');
      var npayload = [];

      for(var i = 0; i < payload.length; i++) {
        // console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
        // console.log(payload[i].uid);
        if(!hashmap.hasOwnProperty(payload[i].uid)) {
          npayload.push(payload[i]);
        }
      }

      // console.log(payload);
      repo.updater(res, req.cookies.auth.email, npayload);
      // do nothing...
      console.log('updated the data base with the new payload!!!!!');
    });
  });

  return router;
};
